package tools

import (
	"bytes"
	"compress/gzip"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	jma "bitbucket.org/hakusan/jma-socket"
)

// XmlTelegram define
type XmlTelegram struct {
	Control Control `xml:"Control"`
	Head    Head    `xml:"Head"`
}
type Control struct {
	Title            string `xml:"Title"`
	DateTime         string `xml:"DateTime"`
	Status           string `xml:"Status"`
	EditorialOffice  string `xml:"EditorialOffice"`
	PublishingOffice string `xml:"PublishingOffice"`
}
type Head struct {
	Title          string `xml:"Title"`
	ReportDateTime string `xml:"ReportDateTime"`
	TargetDateTime string `xml:"TargetDateTime"`
	EventID        string `xml:"EventID"`
}

var (
	v3Keyword = []byte{0x0a, 0x02, 0x0a}
)

/*
 var denbunCategory = map[string]string{
	"ｷﾝｷｭｳｼﾞｼﾝ13": "K0KK13",
	"ﾅｳｷｬｽﾄ13":    "K0KN13",
	"ﾅｳｷｬｽﾄﾘｱﾙ3":  "K0RN03",
	"ﾅｳｷｬｽﾄﾃｽﾄ1":  "K0TN01",
	"ﾅｳｷｬｽﾄﾃｽﾄ91": "K0TN91",
}
*/
var denbunCategory = map[string]string{}

func writeFile(savePath string, filename string, xmlbuff []byte) {
	// ptime := time.Now()
	// y := ptime.Year()
	// ym := ptime.Month()
	// d := ptime.Day()
	// h := ptime.Hour()
	// m := ptime.Minute()
	// s := ptime.Second()
	// xmlFile := fmt.Sprintf("%04d%02d%02d%02d%02d%02d", y, ym, d, h, m, s)
	checkSavePath(savePath)
	count := 1
	xmlFileBuff := filename
	for {
		if _, err := os.Stat(savePath + "/" + xmlFileBuff); err != nil {
			filename = savePath + "/" + xmlFileBuff
			break
		}
		xmlFileBuff = fmt.Sprintf("%d-%s", count, filename)
		count++
	}
	ioutil.WriteFile(filename, xmlbuff, 0644)
}
func checkSavePath(savepath string) (err error) {

	if _, err := os.Stat(savepath); err != nil {
		if err := os.MkdirAll(savepath, 0777); err != nil {
			fmt.Println(err)
		}
	}
	return err
}

// ReadCode : コード電文種別を確定するためのテーブルデータ(S-JIS)を読み込む
func ReadCode() error {
	txtFile, err := os.Open(`./code.txt`)
	if err != nil {
		// Openエラー処理
		return err
	}
	defer txtFile.Close()

	buff, err := ioutil.ReadAll(txtFile)
	if err != nil {
		return err
	}
	class := strings.Split(string(buff), ",")
	for _, val := range class {
		buff := strings.Split(val, ":")
		denbunCategory[buff[0]] = buff[1]
	}
	return nil
}
func StoreTelegram(savepath string, telegram []byte) (xmlbuf []byte, err error) {
	// drop JMA socket header
	rb := telegram[10:]
	// read BCH
	var bch jma.BCH
	err = bch.Set(rb)
	if err != nil {
		return nil, err
	}
	//	bchlen := bch.Length()
	idx := bytes.Index(rb, []byte{0x0a})
	hedding := strings.Split(string(rb[idx+1:idx+14]), " ")
	telCategory, ok := denbunCategory[hedding[0]]
	if !ok {
		telCategory = string(hedding[0])
		if telCategory[0] == 0 {
			telCategory = "K0KN13"
		}
	}
	// v3 code
	if bch.Version() == 3 {
		// BCH + TCH (16byte)
		pt := bch.Length() + 16

		// search 0x0a 0x02 0x0a
		if rb[pt] != 0x0a {
			return nil, errors.New("invalid v3 code")
		}
		for {
			pt++
			tmp := rb[pt : pt+3]
			if bytes.Compare(tmp, v3Keyword) == 0 {
				break
			}
		}
		linebuff := string(rb[pt+3 : len(rb)-1])
		telbuff := strings.Split(linebuff, " ")
		idx = strings.Index(linebuff, "ND") + 2
		var eventid string
		if idx < 10 {
			eventid = "20" + telbuff[3]
		} else {
			eventid = string(linebuff[idx : idx+14])
		}
		savepath = savepath + "/" + eventid[0:4] + "-" + eventid[4:6] + "/" + eventid + "/" + telCategory
		/*
			idx = strings.Index(linebuff, "9999=")
			if idx < 10 {
				idx = strings.Index(linebuff, "9999")
			}
		*/
		idx = strings.LastIndex(linebuff, string(0x0a))
		writeFile(savepath, "20"+telbuff[3]+".txt", []byte(linebuff[:idx+1]))
		return nil, nil
	}

	// v4 unzip
	if bch.Version() == 4 && bch.XMLFlag() == jma.XMLFlagGzip {
		gz, _ := gzip.NewReader(bytes.NewReader(rb[(bch.Length() + bch.ANLength()):]))
		defer gz.Close()
		xmldata, _ := ioutil.ReadAll(gz)
		xmlHead := new(XmlTelegram)
		if err := xml.Unmarshal(xmldata, xmlHead); err != nil {
			return nil, err
		}
		eventid := xmlHead.Head.EventID[0:4] + "-" + xmlHead.Head.EventID[4:6]
		savepath = savepath + "/" + eventid + "/" + xmlHead.Head.EventID + "/" + telCategory
		fd := xmlHead.Head.TargetDateTime
		filename := fd[0:4] + fd[5:7] + fd[8:10] + fd[11:13] + fd[14:16] + fd[17:19] + ".xml"
		writeFile(savepath, string(filename), xmldata)

		return nil, nil
	}

	return nil, errors.New("unpack error")
}
