package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/signal"
	"sync"
	"time"

	tools "bitbucket.org/hakusan/sbv3-strage/tools"
)

// RcStrage define
type RcStrage struct {
	Save    string        `xml:"save"`
	Passive []PassiveInfo `xml:"passive"`
}

// PassiveInfo is properties
type PassiveInfo struct {
	ID         string `xml:"name,attr"`
	Address    string `xml:"address"`
	Port       uint16 `xml:"port"`
	HcInterval int    `xml:"hc_interval"`
	HcTimeout  int    `xml:"hc_timeout"`
	CpTimeout  int    `xml:"cp_timeout"`
}

// ReadConfig is read sample.json file
// and setting initial values
func readConfig(config *RcStrage) error {
	xmlFile, err := os.Open(`./rc_strage.xml`)
	if err != nil {
		// Openエラー処理
		return err
	}
	defer xmlFile.Close()

	buff, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		return err
	}
	if err := xml.Unmarshal(buff, config); err != nil {
		return err
	}
	return nil
}
func readTelegram() ([]byte, error) {
	return ioutil.ReadFile("./telegram.xml")
}

func callPassive(passive PassiveInfo, savePath string, done chan struct{}, wgMain *sync.WaitGroup) {
	serverIP := passive.Address //サーバ側のIP
	serverPort := passive.Port  //サーバ側のポート番号
	addr := fmt.Sprintf("%s:%d", serverIP, serverPort)
	conn, err := net.Dial("tcp", addr)
	checkError(err)
	defer conn.Close()

	readBuf := make([]byte, 1024*4)
	sw := true
	length := 0
	for {
		conn.SetReadDeadline(time.Now().Add(30 * time.Second))
		readlen, err := conn.Read(readBuf)
		if err == nil {
			if sw {
				fmt.Sscanf(string(readBuf[:8]), "%d", &length)

				if readlen > 30 {
					buff := []byte(fmt.Sprintf("%08dENACK", 33))
					buff = append(buff, readBuf[:30]...)
					conn.Write(buff)
					tools.StoreTelegram(savePath, readBuf[:readlen])
				}
				if readlen == 13 {
					conn.Write([]byte(fmt.Sprintf("%08dENCHK", 3)))
				}
				sw = false
			}
			length -= readlen
			if length < 1 {
				sw = true
			}
		} else {
			if err.Error() == "EOF" {
				fmt.Printf("Server:%s close\n", addr)
				wgMain.Done()
				close(done)
				return
			}
		}
		select {
		case <-done:
			wgMain.Done()
			return
		default:
		}
	}
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "fatal: error: %s\n", err.Error())
		os.Exit(1)
	}
}

var done chan struct{}

func main() {
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh)
	done = make(chan struct{})
	RcStrage := new(RcStrage)
	err := readConfig(RcStrage)
	checkError(err)
	if RcStrage.Save == "" {
		RcStrage.Save, _ = os.Getwd()
	}
	if _, err := os.Stat(RcStrage.Save); err != nil {
		if err := os.MkdirAll(RcStrage.Save, 0777); err != nil {
			fmt.Println(err)
		}
	}
	wgMain := &sync.WaitGroup{}
	tools.ReadCode()
	for _, passive := range RcStrage.Passive {
		wgMain.Add(1)
		go callPassive(passive, RcStrage.Save, done, wgMain)
	}
	select {
	case <-sigCh:
		close(done)
	case <-done:
	}
	fmt.Println("App Stop")
	wgMain.Wait()
}
